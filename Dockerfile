#Version 1.2
#Change directory with check scripts

FROM ubuntu:22.10

RUN useradd -m -p password udemy && \
    mkdir /udemy && \
    touch /udemy/fichier1 && \
    mkdir /udemy2 && \
    touch /udemy2/fichier2 && \
    touch /home/udemy/monscript.sh && \
    chmod a+w /home/udemy/monscript.sh && \
    mkdir /home/udemy/check && \
    echo "Bonjour" > /home/udemy/monscript.sh

ADD *.sh /home/udemy/check/

RUN chmod a+x /home/udemy/check/*.sh

CMD ["sleep", "infinity"]