# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes Linux de base.

Ainsi nous verrons dans ce scénario, les commandes :

 - `sudo`: permettre de changer d'utilisateur
 - `pwd`: connaître le répertoire dans lequel je me situe
 - `ls`: lister les fichiers et les répertoires situés dans le répertoire courant
 - `cd`: se déplacer de répertoire en répertoire dans le système
 - `rm`: supprimer un fichier
 - `touch`: créer un fichier
 - `cat`: afficher le contenu d'un fichier

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name commandeslinux jassouline/commandeslinux:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

# Exercice 1
Dans ce premier scénario, nous allons chercher à utiliser la commande su pour changer d'utilisateur.
 
1. Essayez de vous authentifier avec l'utilisateur **udemy** en utilisant la commande `su udemy`

<details><summary> Montrer la solution </summary>
<p>
su udemy
</p>
</details>

<br/>

# Exercice 2
Maintenant que vous êtes authentifié, déterminez dans quel répertoire vous situez-vous.
 
2. Quel est le nom du répertoire dans lequel vous vous situez

<details><summary> Montrer la solution </summary>
<p>
pwd
</p>
<p>
/
</p>
</details>

<br/>

# Exercice 3
3. Déplacez-vous dans le répertoire **/udemy** et identifiez les fichiers qui s'y trouvent.

**Q1: Entrez le nom du fichier qui existe dans le répertoire /udemy**

<details><summary> Montrer la solution </summary>
<p>
cd /udemy && ls
</p>
<p>
fichier1
</p>
</details>


4. Maintenant essayez de faire la même manipulation pour identifier les fichiers qui existent dans le répertoire /udemy2

**Q2: Entrez le nom du fichier qui existe dans le répertoire /udemy2**

<details><summary> Montrer la solution </summary>
<p>
cd /udemy2 && ls
</p>
<p>
fichier2
</p>
</details>

5. Tapez la commande `exit` pour vous déauthentifier de l'utilisateur **udemy** et repasser en utilisateur **root**
   
<br/>

# Exercice 4
6. Déplacez-vous dans le répertoire **/home/udemy** et identifiez les fichiers qui s'y trouvent.

**Q1: Entrez le nom du fichier qui existe dans le répertoire /home/udemy**

<details><summary> Montrer la solution </summary>
<p>
cd /home/udemy
</p>
<p>
ls
</p>
<p>
check monscript.sh
</p>
</details>

**Q2: Quel est le contenu du fichier ?**

<details><summary> Montrer la solution </summary>
<p>
cat /home/udemy/monscript.sh
</p>
<p>
Bonjour
</p>
</details>

<br/>

# Exercice 5

7. Supprimez maintenant le fichier que vous avez identifié dans le répertoire **/home/udemy**

<details><summary> Montrer la solution </summary>
<p>
rm /home/udemy/monscript.sh
</p>
</details>

8. Vérifiez que vous avez correctement effectué l'action en tapant la commande :
`/home/udemy/check/suppression-verify.sh`

Si vous n'obtenez pas le résultat `"OK !"` c'est que vous n'avez pas correctement effectué l'action précédente.

<br/>

# Exercice 6

9. Créer le fichier **jassouline** dans le répertoire **/home/udemy**

<details><summary> Montrer la solution </summary>
<p>
touch /home/udemy/jassouline
</p>
</details>

10. Vérifiez que vous avez correctement effectué l'action en tapant la commande :
`/home/udemy/check/creation-verify.sh`

<br/>

# Conclusion

11. Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

Il est possible que vous ayez à taper la commande deux fois si vous êtes toujours authentifié en tant qu'utilisateur udemy.


